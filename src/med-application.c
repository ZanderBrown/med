/* med-window.c
 *
 * Copyright 2019 Zander Brown
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * SECTION:med-application
 * @title: MedApplication
 * @short_description: Application
 */

#define G_LOG_DOMAIN "Med"

#include <glib/gi18n.h>
#include <gtksourceview/gtksource.h>

#include "med-config.h"
#include "med-application.h"
#include "med-window.h"

G_DEFINE_TYPE (MedApplication, med_application, GTK_TYPE_APPLICATION)

static void
med_application_activate (GApplication *app)
{
  GtkWindow *window;
  guint32 timestamp;

  timestamp = GDK_CURRENT_TIME;

  /* Get the current window or create one if necessary. */
  window = gtk_application_get_active_window (GTK_APPLICATION (app));
  if (window == NULL) {
    window = g_object_new (MED_TYPE_WINDOW,
                           "application", app,
                           NULL);
  }

  gtk_window_present_with_time (window, timestamp);
}

static void
med_application_startup (GApplication *app)
{
  GtkSettings    *gtk_settings;
  GSettings      *settings;
  GtkCssProvider *provider;
  const char *const new_window_accels[] = { "<primary>n", NULL };
  const char *const find_accels[] = { "<primary>f", NULL };
  const char *const save_accels[] = { "<primary>s", NULL };
  const char *const save_as_accels[] = { "<primary><shift>s", NULL };
  const char *const open_accels[] = { "<primary>o", NULL };

  G_APPLICATION_CLASS (med_application_parent_class)->startup (app);

  gtk_source_init ();

  gtk_application_set_accels_for_action (GTK_APPLICATION (app),
                                         "win.new-window", new_window_accels);
  gtk_application_set_accels_for_action (GTK_APPLICATION (app),
                                         "win.find", find_accels);
  gtk_application_set_accels_for_action (GTK_APPLICATION (app),
                                         "win.save", save_accels);
  gtk_application_set_accels_for_action (GTK_APPLICATION (app),
                                         "win.save-as", save_as_accels);
  gtk_application_set_accels_for_action (GTK_APPLICATION (app),
                                         "win.open", open_accels);

  gtk_settings = gtk_settings_get_default ();

  settings = g_settings_new ("org.gnome.zbrown.Med");
  g_settings_bind (settings, "dark",
                   gtk_settings, "gtk-application-prefer-dark-theme",
                   G_SETTINGS_BIND_DEFAULT);

  g_action_map_add_action (G_ACTION_MAP (app),
                           g_settings_create_action (settings, "dark"));

  provider = gtk_css_provider_new ();
  gtk_css_provider_load_from_resource (provider, RES_PATH "styles.css");
  gtk_style_context_add_provider_for_screen (gdk_screen_get_default (),
                                             GTK_STYLE_PROVIDER (provider),
                                             GTK_STYLE_PROVIDER_PRIORITY_APPLICATION );
}

static void
med_application_open (GApplication  *app,
                      GFile        **files,
                      gint           n_files,
                      const gchar   *hint)
{
  for (int i = 0; i < n_files; i++) {
    g_object_new (MED_TYPE_WINDOW,
                  "file", files[i],
                  "handling-open", TRUE,
                  "application", app,
                  "visible", TRUE,
                  NULL);
  }
}

static void
med_application_class_init (MedApplicationClass *klass)
{
  GApplicationClass *app_class    = G_APPLICATION_CLASS (klass);

  app_class->activate = med_application_activate;
  app_class->startup = med_application_startup;
  app_class->open = med_application_open;
}

static void
med_application_init (MedApplication *self)
{
}
