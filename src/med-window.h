/* med-window.h
 *
 * Copyright 2019 Zander Brown
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>
#include <gtksourceview/gtksource.h>

G_BEGIN_DECLS

#define MED_TYPE_WINDOW (med_window_get_type())

/**
 * MedWindow:
 * @file: the #GtkSourceFile for this window
 * @buffer: the open #GtkSourceBuffer being edited
 * @loader: the #GtkSourceFileLoader used to fill @buffer
 * @saver: the #GtkSourceFileSaver used to store @buffer
 * @handling_open: when %TRUE we are being created to open a file
 * @close_anyway: ignore dirty state of @buffer and close the window
 * @inhibit: the cookie from gtk_application_inhibit()
 * @timeout: the id of the #GSource used to hide the statusbar
 * @editor: the #GtkSourceView editor
 * @status: the floating statusbar
 * 
 * Since: 0.1.0
 */
struct _MedWindow
{
  /*< private >*/
  GtkApplicationWindow  parent_instance;

  /*< public >*/
  GtkSourceFile        *file;
  GtkSourceBuffer      *buffer;
  GtkSourceFileLoader  *loader;
  GtkSourceFileSaver   *saver;

  gboolean              handling_open;
  gboolean              close_anyway;
  guint                 inhibit;

  /* Status Bar */
  guint                 timeout;

  /* Template widgets */
  GtkWidget            *editor;
  GtkWidget            *status;
};

G_DECLARE_FINAL_TYPE (MedWindow, med_window, MED, WINDOW, GtkApplicationWindow)

void med_window_show_status (MedWindow  *self,
                             const char *status);

G_END_DECLS
