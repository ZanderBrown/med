/* med-application.h
 *
 * Copyright 2019 Zander Brown
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>

#include "med-window.h"

G_BEGIN_DECLS

#define MED_TYPE_APPLICATION (med_application_get_type())

/**
 * MedApplication:
 * 
 * Stability: Private
 * 
 * Since: 0.1.0
 */
struct _MedApplication
{
  /*< private >*/
  GtkApplication            parent_instance;

  /*< public >*/
};

G_DECLARE_FINAL_TYPE (MedApplication, med_application, MED, APPLICATION, GtkApplication)

G_END_DECLS
