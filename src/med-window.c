/* med-window.c
 *
 * Copyright 2019 Zander Brown
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * SECTION:med-window
 * @title: MedWindow
 * @short_description: Window
 * 
 * The main #GtkApplicationWindow that acts as the editor
 * 
 * Since: 0.1.0
 */

#define G_LOG_DOMAIN "Med"

#include <glib/gi18n.h>

#include "med-config.h"
#include "med-window.h"
#include "med-application.h"


G_DEFINE_TYPE (MedWindow, med_window, GTK_TYPE_APPLICATION_WINDOW)

enum {
  PROP_0,
  PROP_FILE,
  PROP_HANDLING_OPEN,
  LAST_PROP
};

static GParamSpec *pspecs[LAST_PROP] = { NULL, };


static void
update_title (MedWindow  *self,
              char      **document_name,
              gboolean   *unsaved)
{
  g_autoptr (GFileInfo) info = NULL;
  g_autoptr (GError) error = NULL;
  g_autofree char *name = NULL;
  g_autofree char *title = NULL;
  GFile *file;

  file = gtk_source_file_get_location (self->file);

  name = g_strdup (_("Unsaved Document"));

  if (unsaved) {
    *unsaved = TRUE;
  }

  if (file != NULL) {
    info = g_file_query_info (file,
                              G_FILE_ATTRIBUTE_STANDARD_DISPLAY_NAME,
                              G_FILE_QUERY_INFO_NONE,
                              NULL,
                              &error);

    if (error) {
      g_critical ("How?? %s", error->message);
    }

    if (info) {
      g_clear_pointer (&name, g_free);
      name = g_strdup (g_file_info_get_display_name (info));

      if (unsaved) {
        *unsaved = FALSE;
      }
    }
  }

  if (document_name) {
    *document_name = g_strdup (name);
  }
  
  if (unsaved || gtk_text_buffer_get_modified (GTK_TEXT_BUFFER (self->buffer))) {
    title = g_strdup_printf ("%s •", name);
  } else {
    title = g_strdup (name);
  }

  gtk_window_set_title (GTK_WINDOW (self), title);
}

static void
save_progress (goffset  current_num_bytes,
               goffset  total_num_bytes,
               gpointer data)
{
  MedWindow *self = MED_WINDOW (data);
  g_autofree char *status = NULL;

  status = g_strdup_printf (_("Saving… %s/%s"),
                            g_format_size (current_num_bytes),
                            g_format_size (total_num_bytes));

  med_window_show_status (self, status);
}

static void
save_finish (GObject      *source,
             GAsyncResult *res,
             gpointer      data)
{
  MedWindow *self = MED_WINDOW (data);
  g_autoptr (GError) error = NULL;

  if (gtk_source_file_saver_save_finish (GTK_SOURCE_FILE_SAVER (source), res, &error)) {
    gtk_text_buffer_set_modified (GTK_TEXT_BUFFER (self->buffer), FALSE);

    med_window_show_status (self, _("Done"));

    if (self->inhibit > 0) {
      gtk_application_uninhibit (gtk_window_get_application (GTK_WINDOW (self)),
                                self->inhibit);
      self->inhibit = 0;
    }
  } else {
    med_window_show_status (self, _("Save Failed"));

    g_critical ("Failed to save: %s", error->message);
  }

  update_title (self, NULL, NULL);
}

static void
save (MedWindow *self)
{
  gtk_source_file_saver_save_async (self->saver,
                                    G_NOTIFICATION_PRIORITY_NORMAL,
                                    NULL,
                                    save_progress,
                                    self,
                                    NULL,
                                    save_finish,
                                    self);
}

static void
load_progress (goffset  current_num_bytes,
               goffset  total_num_bytes,
               gpointer data)
{
  MedWindow *self = MED_WINDOW (data);
  g_autofree char *status = NULL;

  status = g_strdup_printf (_("Loading… %s/%s"),
                            g_format_size (current_num_bytes),
                            g_format_size (total_num_bytes));

  med_window_show_status (self, status);
}

static void
load_finish (GObject      *source,
             GAsyncResult *res,
             gpointer      data)
{
  MedWindow *self = MED_WINDOW (data);
  g_autoptr (GError) error = NULL;

  if (gtk_source_file_loader_load_finish (GTK_SOURCE_FILE_LOADER (source), res, &error)) {
    gtk_text_buffer_set_modified (GTK_TEXT_BUFFER (self->buffer), FALSE);

    med_window_show_status (self, _("Done"));
  } else {
    if (self->handling_open && g_error_matches (error, G_IO_ERROR, G_IO_ERROR_NOT_FOUND)) {
      gtk_text_buffer_set_modified (GTK_TEXT_BUFFER (self->buffer), TRUE);

      self->handling_open = FALSE;

      med_window_show_status (self, _("Done"));
    } else {
      med_window_show_status (self, _("Load Failed"));

      g_critical ("Failed to load: %s", error->message);
    }
  }

  update_title (self, NULL, NULL);
}

static void
load (MedWindow *self)
{
  gtk_source_file_loader_load_async (self->loader,
                                     G_NOTIFICATION_PRIORITY_NORMAL,
                                     NULL,
                                     load_progress,
                                     self,
                                     NULL,
                                     load_finish,
                                     self);
}

static void
med_window_constructed (GObject *object)
{
  MedWindow *self = MED_WINDOW (object);

  G_OBJECT_CLASS (med_window_parent_class)->constructed (object);

  if (gtk_source_file_get_location (self->file) != NULL) {
    self->loader = gtk_source_file_loader_new (self->buffer, self->file);
    self->saver = gtk_source_file_saver_new (self->buffer, self->file);

    load (self);
  }
}

static void
med_window_set_property (GObject      *object,
                         guint         property_id,
                         const GValue *value,
                         GParamSpec   *pspec)
{
  MedWindow *self = MED_WINDOW (object);

  switch (property_id) {
    case PROP_FILE:
      gtk_source_file_set_location (self->file, g_value_get_object (value));
      break;
    case PROP_HANDLING_OPEN:
      self->handling_open = g_value_get_boolean (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

static void
med_window_get_property (GObject    *object,
                         guint       property_id,
                         GValue     *value,
                         GParamSpec *pspec)
{
  MedWindow *self = MED_WINDOW (object);

  switch (property_id) {
    case PROP_FILE:
      g_value_set_object (value, gtk_source_file_get_location (self->file));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

static void
med_window_dispose (GObject *object)
{
  MedWindow *self = MED_WINDOW (object);

  if (self->inhibit > 0) {
    gtk_application_uninhibit (gtk_window_get_application (GTK_WINDOW (self)),
                               self->inhibit);
    self->inhibit = 0;
  }

  G_OBJECT_CLASS (med_window_parent_class)->dispose (object);
}

static void
med_window_finalize (GObject *object)
{
  MedWindow *self = MED_WINDOW (object);

  g_clear_object (&self->loader);
  g_clear_object (&self->saver);
  g_clear_object (&self->file);

  G_OBJECT_CLASS (med_window_parent_class)->finalize (object);
}

static void
delete_response (GtkWidget *dlg,
                 int        response,
                 MedWindow *self)
{
  gtk_widget_destroy (dlg);

  if (response == GTK_RESPONSE_CANCEL ||
      response == GTK_RESPONSE_DELETE_EVENT) {
    return;
  }

  if (response == GTK_RESPONSE_ACCEPT) {
    self->close_anyway = TRUE;
  } else {
    g_action_group_activate_action (G_ACTION_GROUP (self), "save", NULL);
  }

  gtk_widget_destroy (GTK_WIDGET (self));
}


static gboolean
med_window_delete_event (GtkWidget   *widget,
                         GdkEventAny *event)
{
  MedWindow *self = MED_WINDOW (widget);

  if (gtk_text_buffer_get_modified (GTK_TEXT_BUFFER (self->buffer)) &&
      !self->close_anyway) {
    GtkWidget *dialog;
    GtkWidget *button;
    g_autofree char *name = NULL;

    update_title (self, &name, NULL);

    dialog = gtk_message_dialog_new (GTK_WINDOW (self),
                                     GTK_DIALOG_MODAL,
                                     GTK_MESSAGE_QUESTION,
                                     GTK_BUTTONS_NONE,
                                     _("Save changes to “%s”?"),
                                     name);
    gtk_message_dialog_format_secondary_text (GTK_MESSAGE_DIALOG (dialog),
                                              _("If you don't save, changes will be lost"));

    button = gtk_dialog_add_button (GTK_DIALOG (dialog), _("_Don't Save"), GTK_RESPONSE_ACCEPT);
    gtk_widget_set_can_default (button, FALSE);
    button = gtk_dialog_add_button (GTK_DIALOG (dialog), _("_Cancel"), GTK_RESPONSE_CANCEL);
    gtk_widget_set_can_default (button, FALSE);
    button = gtk_dialog_add_button (GTK_DIALOG (dialog), _("_Save"), GTK_RESPONSE_APPLY);
    gtk_widget_set_can_default (button, TRUE);
    gtk_widget_grab_focus (button);

    g_signal_connect (dialog, "response", G_CALLBACK (delete_response), self);

    gtk_widget_show (dialog);

    return TRUE; // Hold on a sec
  }

  return FALSE; // Aka no, I don't want to block closing
}

static void
application_set (GObject *object, GParamSpec *pspec, gpointer data)
{
}

static void
buffer_changed (GtkTextBuffer *buffer,
                MedWindow     *self)
{
  GtkApplication *app;
  g_autofree char *reason = NULL;
  g_autofree char *name = NULL;
  gboolean unsaved = FALSE;

  update_title (self, &name, &unsaved);

  app = gtk_window_get_application (GTK_WINDOW (self));

  if ((gtk_text_buffer_get_modified (GTK_TEXT_BUFFER (self->buffer)) || unsaved) &&
         self->inhibit == 0) {
    if (self->saver == NULL) {
      reason = g_strdup (_("Document has unsaved changes"));
    } else {
      reason = g_strdup_printf (_("“%s” has unsaved changes"), name);
    }

    self->inhibit = gtk_application_inhibit (app,
                                             GTK_WINDOW (self),
                                             GTK_APPLICATION_INHIBIT_LOGOUT,
                                             reason);
  }
}

static void
med_window_class_init (MedWindowClass *klass)
{
  GObjectClass   *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->constructed = med_window_constructed;
  object_class->set_property = med_window_set_property;
  object_class->get_property = med_window_get_property;
  object_class->dispose = med_window_dispose;
  object_class->finalize = med_window_finalize;

  widget_class->delete_event = med_window_delete_event;

  /**
   * MedWindow:file:
   * 
   * File open in the window
   * 
   * Can only be set at construction but may change if the users does save as
   * 
   * Stability: Private
   * 
   * Since: 0.1.0
   */
  pspecs[PROP_FILE] =
    g_param_spec_object ("file", "File", "The open file",
                         G_TYPE_FILE,
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);

  /**
   * MedWindow:handling-open:
   * 
   * %TRUE when #MedWindow created whilst handling
   * #GApplication::open on #GApplication
   * 
   * Stability: Private
   * 
   * Since: 0.1.0
   */
  pspecs[PROP_HANDLING_OPEN] =
    g_param_spec_boolean ("handling-open", "handling open", "We are responding to ::open",
                          FALSE,
                          G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY);

  g_object_class_install_properties (object_class, LAST_PROP, pspecs);

  gtk_widget_class_set_template_from_resource (widget_class,
                                               RES_PATH "med-window.ui");

  gtk_widget_class_bind_template_child (widget_class, MedWindow, editor);
  gtk_widget_class_bind_template_child (widget_class, MedWindow, status);
  gtk_widget_class_bind_template_child (widget_class, MedWindow, buffer);

  gtk_widget_class_bind_template_callback (widget_class, application_set);
  gtk_widget_class_bind_template_callback (widget_class, buffer_changed);
}

static void
new_activated (GSimpleAction *action,
               GVariant      *parameter,
               gpointer       data)
{
  GtkWindow       *window = NULL;
  GtkApplication  *app = NULL;
  guint32          timestamp;

  /* Slightly "wrong" but hopefully by taking the time before
   * we spend non-zero time initing the window it's far enough in the
   * past for shell to do-the-right-thing
   */
  timestamp = GDK_CURRENT_TIME;

  app = gtk_window_get_application (GTK_WINDOW (data));

  window = g_object_new (MED_TYPE_WINDOW,
                         "application", app,
                         NULL);

  gtk_window_present_with_time (window, timestamp);
}

static void
about_activated (GSimpleAction *action,
                 GVariant      *parameter,
                 gpointer       data)
{
  const char *authors[] = { "Zander Brown <zbrown@gnome.org>", NULL };
  g_autofree char *copyright = NULL;
  
  copyright = g_strdup_printf (_("Copyright © %s Zander Brown"),
                               "2019");

  gtk_show_about_dialog (GTK_WINDOW (data),
                         "authors", authors,
                         // Translators: Credit yourself here
                         "translator-credits", _("translator-credits"),
                         "comments", _("Text Editor"),
                         "copyright", copyright,
                         "license-type", GTK_LICENSE_GPL_3_0,
                         "logo-icon-name", "org.gnome.zbrown.Med",
                         // Translators: Med = Minimal Editor
                         "program-name", _("Med"),
                         "version", PACKAGE_VERSION,
                         NULL);
}

static void
save_as_activated (GSimpleAction *action,
                   GVariant      *parameter,
                   gpointer       data)
{
  MedWindow *self = MED_WINDOW (data);
  g_autoptr (GtkFileChooserNative) chooser = NULL;
  g_autoptr (GFile) file = NULL;

  chooser = gtk_file_chooser_native_new (_("Save File"),
                                         GTK_WINDOW (self),
                                         GTK_FILE_CHOOSER_ACTION_SAVE,
                                         _("_Save"),
                                         NULL);
  gtk_file_chooser_set_do_overwrite_confirmation (GTK_FILE_CHOOSER (chooser),
                                                  TRUE);
  gtk_file_chooser_set_current_name (GTK_FILE_CHOOSER (chooser),
                                     _("Document.txt"));

  if (gtk_native_dialog_run (GTK_NATIVE_DIALOG (chooser)) != GTK_RESPONSE_ACCEPT) {
    return;
  }

  file = gtk_file_chooser_get_file (GTK_FILE_CHOOSER (chooser));

  g_clear_object (&self->loader);
  g_clear_object (&self->saver);

  gtk_source_file_set_location (self->file, file);
  self->loader = gtk_source_file_loader_new (self->buffer, self->file);
  self->saver = gtk_source_file_saver_new (self->buffer, self->file);

  save (self);

  g_object_notify_by_pspec (G_OBJECT (self), pspecs[PROP_FILE]);
}

static void
save_activated (GSimpleAction *action,
                GVariant      *parameter,
                gpointer       data)
{
  MedWindow *self = MED_WINDOW (data);

  if (self->saver) {
    save (self);
  } else {
    // Pretend this is save as
    save_as_activated (action, parameter, data);
  }
}

static void
open_activated (GSimpleAction *action,
                GVariant      *parameter,
                gpointer       data)
{
  MedWindow *self = MED_WINDOW (data);
  g_autoptr (GtkFileChooserNative) chooser = NULL;
  g_autoptr (GFile) file = NULL;

  chooser = gtk_file_chooser_native_new (_("Open File"),
                                         GTK_WINDOW (self),
                                         GTK_FILE_CHOOSER_ACTION_OPEN,
                                         _("_Open"),
                                         NULL);

  if (gtk_native_dialog_run (GTK_NATIVE_DIALOG (chooser)) != GTK_RESPONSE_ACCEPT) {
    return;
  }

  file = gtk_file_chooser_get_file (GTK_FILE_CHOOSER (chooser));

  // If the buffer is dirty or backed by a file
  if (gtk_text_buffer_get_modified (GTK_TEXT_BUFFER (self->buffer)) ||
      self->saver != NULL) {
    // Open in a new window
    g_object_new (MED_TYPE_WINDOW,
                  "file", file,
                  "application", gtk_window_get_application (GTK_WINDOW (self)),
                  "visible", TRUE,
                  NULL);
  } else {
    gtk_source_file_set_location (self->file, file);
    self->loader = gtk_source_file_loader_new (self->buffer, self->file);
    self->saver = gtk_source_file_saver_new (self->buffer, self->file);

    load (self);

    g_object_notify_by_pspec (G_OBJECT (self), pspecs[PROP_FILE]);
  }
}

static GActionEntry win_entries[] =
{
  { "new-window", new_activated, NULL, NULL, NULL },
  { "about", about_activated, NULL, NULL, NULL },
  { "save-as", save_as_activated, NULL, NULL, NULL },
  { "save", save_activated, NULL, NULL, NULL },
  { "open", open_activated, NULL, NULL, NULL },
};

static void
med_window_init (MedWindow *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  self->file = gtk_source_file_new ();
  self->loader = NULL;
  self->saver = NULL;
  self->handling_open = FALSE;
  self->inhibit = 0;
  self->close_anyway = FALSE;

  g_action_map_add_action_entries (G_ACTION_MAP (self),
                                   win_entries,
                                   G_N_ELEMENTS (win_entries),
                                   self);
}


static gboolean
statusbar_timeout (MedWindow *self)
{
  self->timeout = 0;

  gtk_widget_hide (self->status);

  return G_SOURCE_REMOVE;
}

void
med_window_show_status (MedWindow  *self,
                        const char *status)
{
  if (self->timeout != 0) {
    g_source_remove (self->timeout);
  }
  self->timeout = g_timeout_add (1000, G_SOURCE_FUNC (statusbar_timeout), self);

  gtk_label_set_label (GTK_LABEL (self->status), status);
  gtk_widget_show (self->status);
}
